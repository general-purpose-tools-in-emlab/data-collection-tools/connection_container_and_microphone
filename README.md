# 'connection_container_and_microphone'
HSR SDE (Docker container)とmicrophoneを接続し、ROSでmessageをキャプチャする方法.  

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

**Content:**
*   [Environment](#Environment)
*   [Setting](#Setting)

## Environment (Confirmed)
```
Ubuntu: 20.04LTS  
ROS: Noetic  
Python: 3.8.10  
```

## Setting
- `apt-get install ros-noetic-audio-common`  
- `apt-get install ros-noetic-audio-common-msgs`  
- `apt-get install ros-noetic-audio-capture`

`docker-compose.yaml`に以下を設定.  
```
services:  
  hsr:  
    devices:  
        - "/dev/snd:/dev/snd"  
```

microphoneとpcをusebで接続.  
hostPCのterminalで`arecord -l`を行い、接続の確認.  
dockerコンテナに入る.  

コンテナ内で`arecord -l`  
もしコンテナでダメなら`docker stop`してからやり直す.   
```
**** List of CAPTURE Hardware Devices ****  
card 0: PCH [HDA Intel PCH], device 0: ALC3861 Analog [ALC3861 Analog]  
  Subdevices: 1/1  
  Subdevice #0: subdevice #0  
card 0: PCH [HDA Intel PCH], device 2: ALC3861 Alt Analog [ALC3861 Alt Analog]  
  Subdevices: 2/2  
  Subdevice #0: subdevice #0  
  Subdevice #1: subdevice #1  
card 2: J710 [Jabra Speak 710], device 0: USB Audio [USB Audio]  
  Subdevices: 1/1  
  Subdevice #0: subdevice #0  
のように表示される.  
```

表示されたデバイスリストから、Jabra Speak 710がカード2、サブデバイス0に存在することが確認できます。  
したがって、Jabra Speak 710を使用するためのデバイス指定は"hw:2,0"となります。  

`roslaunch audio_capture capture.launch device:="hw:2,0"` 

`/audio/audio_stamped`というトピックが`rostopic list`で見れる.  
AudioDataStamped型  
あとはrosbagでrecordが可能になる.  
